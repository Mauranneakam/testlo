package org.example.packgeObj;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends Page {
    @FindBy(id = "style_avatar_wrapper_pEGIQ")
    private WebElement avatar;

    @FindBy(id = "email_login")
    private WebElement email_login;
    @FindBy(id = "password_login")
    private WebElement password_login;

    @FindBy(id = "btn_login")
    private WebElement connect_login;
    private String URL = "https://ztrain-web.vercel.app/home";
    public void navigateToHomePage (){
        this.get(URL);
    }
//    public void fillEmail
}
