import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class FirstTest {
    private WebDriver driver;
    private String path = "src/test/resources/drivers/chromedriver-win64/chromedriver-win64/chromedriver.exe";

    private By email_login = By.className("style_input__dv2f");
    @BeforeTest()
    public void setUp(){
        System.setProperty("webdriver.chrome.drivers", this.path);
        this.driver = new ChromeDriver();
    }
    @Test()
    public void navigateToHomePage(){

        this.driver.get("https://ztrain-web.vercel.app/en/home");
//        this.driver.findElement(By.cssSelector("nav>div:nth-child(4)")).click();
//        this.driver.findElement(By.className("style_avatar_wrapper__pEGIQ"));
//        this.driver.findElement(By.id(".MuiTab-scroller>div:nth-child(2)")).click();
//        this.driver.findElement(By.id("email_login")).sendKeys("momom14@gmail.com");
//        this.driver.findElement(By.id("password_login")).sendKeys("1598746321");
//        this.driver.findElement(By.id("btn_login")).click();
//        this.driver.findElement(By.id("btn_login")).click();

        Assert.assertTrue(this.driver.findElement(By.id("btn_login")).isDisplayed());
    }

//    @AfterTest()
//    public void closeDriver(){
//        this.driver.quit();
//    }
}